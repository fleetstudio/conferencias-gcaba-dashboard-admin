import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { ConferenceRoutes } from './ConferenceRoutes';

export const Routes = () => (
  <Switch>
    <Route exact path="/" render={() => <Redirect to="/conferences" />} />
    <Route path="/conferences" component={ConferenceRoutes} />
    <Route path="/" render={() => <Redirect to="/conferences" />} />
  </Switch>
);
