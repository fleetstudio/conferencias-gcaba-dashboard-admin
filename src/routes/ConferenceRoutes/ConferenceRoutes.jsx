import React from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';
import { ConferenceList, ConferenceView, ConferenceForm } from './routes';

export const ConferenceRoutes = ({ match }) => (
  <Switch>
    <Route exact path={match.url} component={ConferenceList} />
    <Route path={`${match.url}/:id/edit`} component={ConferenceForm} />
    <Route path={`${match.url}/:id`} component={ConferenceView} />
  </Switch>
);

ConferenceRoutes.propTypes = {
  match: PropTypes.shape().isRequired,
};

ConferenceRoutes.defaultProps = {};
