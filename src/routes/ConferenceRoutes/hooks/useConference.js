import { useCallback, useEffect, useState } from 'react';
import { useSnackbar } from 'notistack';
import { Api } from '../../../services/api';
import { conferenceMap } from '../utils';
import { useGlobalContext } from '../../../hooks';
import { getErrorMessage } from '../../../utils/helpers';
import { useTranslation } from '../../../services/translation';
import { isNew } from '../routes/ConferenceForm/utils';

export const useConference = (conferenceId, { skipSigned = false, skipAutoFetch = false } = {}) => {
  const { t } = useTranslation();
  const { enqueueSnackbar } = useSnackbar();
  const { setIsLoading } = useGlobalContext();
  const [conference, setConference] = useState({});
  const fetchConference = useCallback(async (id) => {
    try {
      setIsLoading(true);
      const [conferenceData, signedUsers] = await Promise.all([
        Api.getConferenceById(id),
        skipSigned ? null : Api.getSignedUsersByConference(id),
      ]);
      if (conferenceData) {
        const mapped = conferenceMap(conferenceData);
        if (signedUsers) {
          mapped.signedUsers = signedUsers;
        }
        setConference(mapped);
      }
    } catch (e) {
      const message = getErrorMessage(e);
      enqueueSnackbar(t(message || 'Something is wrong'), { variant: 'error' });
    } finally {
      setIsLoading(false);
    }
  }, [setConference, skipSigned, setIsLoading, enqueueSnackbar, t]);
  useEffect(() => {
    if (!skipAutoFetch && !isNew(conferenceId)) {
      fetchConference(conferenceId);
    }
  }, [fetchConference, conferenceId, skipAutoFetch]);

  return { conference, fetchConference };
};
