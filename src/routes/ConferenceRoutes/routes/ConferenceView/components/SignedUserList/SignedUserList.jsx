import React, { useState, useMemo } from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/styles';
import { Toolbar } from './components/Toolbar';
import { List } from './components/List';
import { filterByKey } from '../../../../../../utils/helpers';
import { useTranslation } from '../../../../../../services/translation';

const useStyles = makeStyles({
  root: {
    marginTop: 32,
  },
});

export const SignedUserList = ({
  users, isExpired, conference, fetchConference,
}) => {
  const { t } = useTranslation();
  const classes = useStyles();
  const [search, setSearch] = useState('');
  const filteredUsers = useMemo(
    () => users.filter(filterByKey(search)),
    [users, search],
  );
  return (
    <div className={classes.root}>
      <Typography variant="h4" gutterBottom>{t('Signed Users')}</Typography>
      <Toolbar search={search} onSearchChange={setSearch} />
      <List
        users={filteredUsers}
        isExpired={isExpired}
        conference={conference}
        fetchConference={fetchConference}
      />
    </div>
  );
};

SignedUserList.propTypes = {
  users: PropTypes.arrayOf(PropTypes.shape()),
  isExpired: PropTypes.bool,
  conference: PropTypes.shape().isRequired,
  fetchConference: PropTypes.func.isRequired,
};

SignedUserList.defaultProps = {
  users: [],
  isExpired: false,
};
