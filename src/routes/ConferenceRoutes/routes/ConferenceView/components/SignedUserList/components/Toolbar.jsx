import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/styles';
import { SearchBar } from '../../../../../../../components/SearchBar';

const useStyles = makeStyles({
  toolbar: {
    marginBottom: 16,
  },
});

export const Toolbar = ({ search, onSearchChange }) => {
  const classes = useStyles();
  return (
    <Grid container alignItems="center" spacing={1} className={classes.toolbar}>
      <Grid item xs={12}>
        <SearchBar value={search} onChange={onSearchChange} />
      </Grid>
    </Grid>
  );
};

Toolbar.propTypes = {
  search: PropTypes.string,
  onSearchChange: PropTypes.func,
};

const dummy = () => {};
Toolbar.defaultProps = {
  search: '',
  onSearchChange: dummy,
};
