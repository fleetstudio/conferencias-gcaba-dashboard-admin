import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/styles';
import { useSnackbar } from 'notistack';
import { ListBase } from '../../../../../../../../components/ListBase';
import { UnsubscribeAction } from './components/UnsubscribeAction';
import { useGlobalContext } from '../../../../../../../../hooks';
import { useTranslation } from '../../../../../../../../services/translation';
import { Api } from '../../../../../../../../services/api';
import { getErrorMessage } from '../../../../../../../../utils/helpers';

const renderPrimary = (item) => item.name;

const useStyles = makeStyles((theme) => ({
  listItem: {
    marginBottom: 16,
    backgroundColor: theme.palette.common.white,
  },
}));

export const List = ({
  users, isExpired, conference, fetchConference,
}) => {
  const { setIsLoading } = useGlobalContext();
  const { enqueueSnackbar } = useSnackbar();
  const { t } = useTranslation();
  const classes = useStyles();
  const onUnsubscribeClick = useCallback(async (user) => {
    try {
      setIsLoading(true);
      await Api.removeInscriptionById(conference.id, user.id);
      setIsLoading(false);
      fetchConference(conference.id);
    } catch (e) {
      const message = getErrorMessage(e);
      enqueueSnackbar(t(message || 'Something is wrong'), { variant: 'error' });
      setIsLoading(false);
    }
  }, [conference, setIsLoading, t, enqueueSnackbar, fetchConference]);
  const renderAction = useCallback(
    (item) => <UnsubscribeAction user={item} isExpired={isExpired} onClick={onUnsubscribeClick} />,
    [isExpired, onUnsubscribeClick],
  );
  return (
    <ListBase
      items={users}
      renderPrimary={renderPrimary}
      renderAction={renderAction}
      listItemProps={{ component: Paper, className: classes.listItem }}
    />
  );
};

List.propTypes = {
  users: PropTypes.arrayOf(PropTypes.shape()),
  isExpired: PropTypes.bool,
  conference: PropTypes.shape().isRequired,
  fetchConference: PropTypes.func.isRequired,
};

List.defaultProps = {
  users: [],
  isExpired: false,
};
