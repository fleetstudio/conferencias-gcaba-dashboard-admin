import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import { useTranslation } from '../../../../../../../../../services/translation';
import confirmDialog from '../../../../../../../../../services/confirmDialog';

export const UnsubscribeAction = ({
  user, onClick, isExpired,
}) => {
  const { t } = useTranslation();

  const onUnsubscribeClick = useCallback(async () => {
    const confirm = await confirmDialog.show({
      title: t('Unsubscribe Confirmation'),
      content: `${t('Do you want to unsubscribe')} "${user.name}" ?`,
      confirmText: `${t('Unsubscribe')}!`,
      cancelText: t('No'),
    });
    if (confirm && onClick) {
      onClick(user);
    }
  }, [user, onClick, t]);
  return (
    <IconButton
      disabled={isExpired}
      onClick={onUnsubscribeClick}
    >
      <DeleteIcon />
    </IconButton>
  );
};

UnsubscribeAction.propTypes = {
  user: PropTypes.shape().isRequired,
  isExpired: PropTypes.bool,
  onClick: PropTypes.func,
};

UnsubscribeAction.defaultProps = {
  onClick: null,
  isExpired: false,
};
