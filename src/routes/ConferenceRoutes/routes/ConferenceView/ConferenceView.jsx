import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import moment from 'moment';
import { useSnackbar } from 'notistack';
import { Screen } from '../../../../components/Screen';
import { ScreenTitle } from '../../../../components/ScreenTitle';
import { useTranslation } from '../../../../services/translation';
import { useConference } from '../../hooks';
import { LeftAndRight } from '../../../../components/LeftAndRight';
import { BasicInfo } from './components/BasicInfo';
import { SignedUserList } from './components/SignedUserList';
import { EditDeleteConferenceAction } from '../../../../components/EditDeleteConferenceAction';
import { useGlobalContext } from '../../../../hooks';
import { Api } from '../../../../services/api';
import { getErrorMessage } from '../../../../utils/helpers';

const useStyles = makeStyles((theme) => ({
  footer: {
    position: 'fixed',
    top: 'auto',
    bottom: 0,
    left: 0,
    right: 0,
    paddingBottom: 8,
    paddingTop: 8,
    backgroundColor: theme.palette.background.default,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    alignContent: 'center',
  },
  screen: {
    paddingBottom: 52 + 16,
  },
}));
const isAfterNow = (conference) => moment(conference.date).isSameOrAfter(moment());

export const ConferenceView = ({ match, history }) => {
  const { params: { id } } = match;
  const { enqueueSnackbar } = useSnackbar();
  const { setIsLoading } = useGlobalContext();
  const classes = useStyles();
  const { t } = useTranslation();
  const { conference, fetchConference } = useConference(id);
  const onEditClick = useCallback(
    () => history.push(`${match.url}/edit`),
    [history, match],
  );
  const onRemoveClick = useCallback(
    async (item) => {
      try {
        setIsLoading(true);
        await Api.removeConferenceById(item.id);
        setIsLoading(false);
        history.push('/conferences');
      } catch (e) {
        const message = getErrorMessage(e);
        enqueueSnackbar(t(message || 'Something is wrong'), { variant: 'error' });
        setIsLoading(false);
      }
    },
    [setIsLoading, history, enqueueSnackbar, t],
  );
  return (
    <Screen className={classes.screen} hideFooter>
      <ScreenTitle title={t('Conference')} />
      {!!Object.keys(conference).length && (
        <LeftAndRight
          left={<BasicInfo conference={conference} />}
          right={(
            <EditDeleteConferenceAction
              conference={conference}
              onEditClick={onEditClick}
              onRemoveClick={onRemoveClick}
            />
          )}
        />
      )}
      {conference.signedUsers && (
        <SignedUserList
          users={conference.signedUsers}
          isExpired={!isAfterNow(conference)}
          conference={conference}
          fetchConference={fetchConference}
        />
      )}
      <div className={classes.footer}>
        <EditDeleteConferenceAction
          conference={conference}
          onEditClick={onEditClick}
          onRemoveClick={onRemoveClick}
          flipBreakpoint
        />
      </div>
    </Screen>
  );
};

ConferenceView.propTypes = {
  match: PropTypes.shape().isRequired,
  history: PropTypes.shape().isRequired,
};

ConferenceView.defaultProps = {};
