import { useCallback, useMemo } from 'react';
import { useForm } from 'react-final-form-hooks';
import { useSnackbar } from 'notistack';
import { FORM_ERROR } from 'final-form';
import { useConference } from '../../../hooks';
import { isNew } from '../utils';
import { getErrorMessage, makeValidator } from '../../../../../utils/helpers';
import { Api } from '../../../../../services/api';

const validate = makeValidator([
  { key: 'name', validators: ['required'] },
  { key: 'description', validators: ['required'] },
  { key: 'available_positions', validators: ['required', 'positive'] },
  { key: 'date', validators: ['required', 'date'] },
]);

export const useConferenceForm = (conferenceId, t, history) => {
  const { enqueueSnackbar } = useSnackbar();
  const { conference } = useConference(conferenceId, { skipSigned: true });

  const initialValues = useMemo(() => {
    if (isNew(conferenceId)) {
      return {
        name: '', description: '', available_positions: '', date: null, enabled: true,
      };
    }
    return !conference ? {} : conference;
  }, [conference, conferenceId]);

  const onSubmit = useCallback(async (values) => {
    try {
      const updatedConference = isNew(conferenceId)
        ? await Api.createConference(values)
        : await Api.updateConferenceById(conferenceId, values);
      enqueueSnackbar(t('Saved'), { variant: 'success' });
      history.replace(`/conferences/${updatedConference.id}`);
      return {};
    } catch (e) {
      const message = getErrorMessage(e);
      enqueueSnackbar(t(message || 'Something is wrong'), { variant: 'error' });
      return { [FORM_ERROR]: message };
    }
  }, [t, enqueueSnackbar, history, conferenceId]);

  return useForm({
    initialValues,
    onSubmit,
    validate,
  });
};
