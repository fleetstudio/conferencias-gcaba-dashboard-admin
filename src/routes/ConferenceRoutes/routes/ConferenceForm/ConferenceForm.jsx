import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import CircularProgress from '@material-ui/core/CircularProgress';
import { DateTimePicker } from '@material-ui/pickers';
import { useField } from 'react-final-form-hooks';
import { useTranslation } from '../../../../services/translation';
import { Screen } from '../../../../components/Screen';
import { ScreenTitle } from '../../../../components/ScreenTitle';
import { isNew } from './utils';
import { useConferenceForm } from './hooks/useConferenceForm';
import { makeGetErrorAndHelperText, textFieldProps } from '../../../../utils/materialHelpers';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export const ConferenceForm = ({ match, history }) => {
  const { params: { id } } = match;
  const { t } = useTranslation();
  const classes = useStyles();
  const { title, buttonText } = isNew(id) ? {
    title: t('New Conference'),
    buttonText: t('Create it'),
  } : {
    title: t('Edit Conference'),
    buttonText: t('Save it'),
  };
  const {
    form, handleSubmit, submitting, pristine,
  } = useConferenceForm(id, t, history);
  const name = useField('name', form);
  const description = useField('description', form);
  const availablePositions = useField('available_positions', form);
  const date = useField('date', form);
  const getErrorAndHelperText = makeGetErrorAndHelperText(t);
  return (
    <Screen>
      <ScreenTitle title={title} />
      <Grid container justify="center">
        <Grid item md={6} sm={12} xs={12}>
          <form className={classes.form} noValidate onSubmit={handleSubmit}>
            <TextField
              {...textFieldProps(t('Name'))}
              {...name.input}
              {...getErrorAndHelperText(name)}
              autoFocus
            />
            <DateTimePicker
              {...textFieldProps(t('Date'))}
              {...date.input}
              {...getErrorAndHelperText(date)}
              inputVariant="outlined"
              disablePast
              showTodayButton
            />
            <TextField
              {...textFieldProps(t('Participants'))}
              {...availablePositions.input}
              {...getErrorAndHelperText(availablePositions)}
              type="number"
            />
            <TextField
              {...textFieldProps(t('Description'))}
              {...description.input}
              {...getErrorAndHelperText(description)}
              multiline
              rows={5}
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              disabled={pristine || submitting}
            >
              {submitting
                ? <CircularProgress size={24} />
                : buttonText}
            </Button>
          </form>
        </Grid>
      </Grid>
    </Screen>
  );
};

ConferenceForm.propTypes = {
  match: PropTypes.shape().isRequired,
  history: PropTypes.shape().isRequired,
};

ConferenceForm.defaultProps = {};
