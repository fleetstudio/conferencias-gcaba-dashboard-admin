import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/styles';
import { useHistory } from 'react-router-dom';
import { useSnackbar } from 'notistack';
import { ListBase } from '../../../../../components/ListBase';
import { EditDeleteConferenceAction } from '../../../../../components/EditDeleteConferenceAction';
import { Api } from '../../../../../services/api';
import { getErrorMessage } from '../../../../../utils/helpers';
import { useGlobalContext } from '../../../../../hooks';
import { useTranslation } from '../../../../../services/translation';

const renderPrimary = (item) => item.name;
const renderSecondary = (item) => item.dateString;

const useStyles = makeStyles((theme) => ({
  listItem: {
    marginBottom: 16,
    backgroundColor: theme.palette.common.white,
  },
}));

export const List = ({ conferences, match, fetchConferences }) => {
  const { setIsLoading } = useGlobalContext();
  const { enqueueSnackbar } = useSnackbar();
  const { t } = useTranslation();
  const classes = useStyles();
  const history = useHistory();
  const onItemClick = useCallback(
    (item) => history.push(`${match.url}/${item.id}`),
    [history, match],
  );
  const onEditClick = useCallback(
    (item) => history.push(`${match.url}/${item.id}/edit`),
    [history, match],
  );
  const onRemoveClick = useCallback(
    async (item) => {
      try {
        setIsLoading(true);
        await Api.removeConferenceById(item.id);
        setIsLoading(false);
        fetchConferences();
      } catch (e) {
        const message = getErrorMessage(e);
        enqueueSnackbar(t(message || 'Something is wrong'), { variant: 'error' });
        setIsLoading(false);
      }
    },
    [setIsLoading, fetchConferences, enqueueSnackbar, t],
  );
  const renderAction = useCallback(
    (item) => (
      <EditDeleteConferenceAction
        conference={item}
        onEditClick={onEditClick}
        onRemoveClick={onRemoveClick}
      />
    ),
    [onEditClick, onRemoveClick],
  );
  return (
    <ListBase
      onItemClick={onItemClick}
      items={conferences}
      renderPrimary={renderPrimary}
      renderSecondary={renderSecondary}
      renderAction={renderAction}
      listItemProps={{ component: Paper, className: classes.listItem }}
    />
  );
};

List.propTypes = {
  conferences: PropTypes.arrayOf(PropTypes.shape()),
  match: PropTypes.shape().isRequired,
  fetchConferences: PropTypes.func.isRequired,
};

List.defaultProps = {
  conferences: [],
};
