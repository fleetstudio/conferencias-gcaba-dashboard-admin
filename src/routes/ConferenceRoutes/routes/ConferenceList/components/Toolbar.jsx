import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import { makeStyles } from '@material-ui/styles';
import { SearchBar } from '../../../../../components/SearchBar';

const useStyles = makeStyles({
  toolbar: {
    marginBottom: 16,
  },
});

export const Toolbar = ({ search, onSearchChange, onAddClick }) => {
  const classes = useStyles();
  return (
    <Grid container alignItems="center" spacing={1} className={classes.toolbar}>
      <Grid item xs sm lg>
        <SearchBar value={search} onChange={onSearchChange} />
      </Grid>
      <Grid item>
        <IconButton onClick={onAddClick}>
          <AddIcon />
        </IconButton>
      </Grid>
    </Grid>
  );
};

Toolbar.propTypes = {
  search: PropTypes.string,
  onSearchChange: PropTypes.func,
  onAddClick: PropTypes.func,
};

const dummy = () => {};
Toolbar.defaultProps = {
  search: '',
  onSearchChange: dummy,
  onAddClick: dummy,
};
