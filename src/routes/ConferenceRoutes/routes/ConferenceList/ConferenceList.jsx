import React, { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import { Screen } from '../../../../components/Screen';
import { useTranslation } from '../../../../services/translation';
import { useConferences } from '../../hooks';
import { List } from './components/List';
import { ScreenTitle } from '../../../../components/ScreenTitle';
import { Toolbar } from './components/Toolbar';

// TODO: skip deleted_at to student dashboard but no in the admin
export const ConferenceList = ({ match, history }) => {
  const { t } = useTranslation();
  const [search, setSearch] = useState('');
  const { conferences, fetchConferences } = useConferences(search);
  const onAddClick = useCallback(
    () => history.push(`${match.url}/new/edit`),
    [history, match],
  );
  return (
    <Screen>
      <ScreenTitle title={t('Our Conferences')} />
      <Toolbar search={search} onSearchChange={setSearch} onAddClick={onAddClick} />
      <List conferences={conferences} match={match} fetchConferences={fetchConferences} />
    </Screen>
  );
};

ConferenceList.propTypes = {
  match: PropTypes.shape().isRequired,
  history: PropTypes.shape().isRequired,
};

ConferenceList.defaultProps = {};
