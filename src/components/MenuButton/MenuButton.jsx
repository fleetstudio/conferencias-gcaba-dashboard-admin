import React, { useCallback } from 'react';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { useGlobalContext } from '../../hooks';

export const MenuButton = () => {
  const { set } = useGlobalContext();
  const openDrawer = useCallback(() => {
    set(true);
  }, [set]);
  return (
    <IconButton color="inherit" aria-label="Open menu" onClick={openDrawer}>
      <MenuIcon />
    </IconButton>
  );
};

MenuButton.propTypes = {};

MenuButton.defaultProps = {};
