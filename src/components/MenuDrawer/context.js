import React from 'react';

const MenuDrawerContext = React.createContext(false);

export default MenuDrawerContext;
