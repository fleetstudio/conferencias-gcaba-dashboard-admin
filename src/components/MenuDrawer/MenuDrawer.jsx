import React, { useCallback } from 'react';
import { Link } from 'react-router-dom';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import Avatar from '@material-ui/core/Avatar';
import ExpensesIcon from '@material-ui/icons/List';
import LogoutIcon from '@material-ui/icons/ExitToApp';
import MenuDrawerItem from './components/MenuDrawerItem';
import { AuthService } from '../../services/auth';
import useStyles from './styles';
import { useTranslation } from '../../services/translation';
import dummyAvatar from '../../assets/images/dummy-avatar.jpg';
import { useGlobalContext } from '../../hooks';

export const MenuDrawer = () => {
  const { t } = useTranslation();
  // Styles
  const classes = useStyles();
  const {
    isOpen, set, userInfo, isLogged,
  } = useGlobalContext();
  // Handlers
  const onCloseDrawer = useCallback(() => {
    set(false);
  }, [set]);
  const { avatar_url: avatarUrl, name } = userInfo || {};
  return (
    <Drawer open={isOpen} onClose={onCloseDrawer}>
      <div role="presentation" onClick={onCloseDrawer}>
        <div className={classes.list}>
          <div className={classes.toolbar}>
            <Typography variant="h6">Menu</Typography>
          </div>
          <Divider />
          {isLogged && (
            <div className={classes.profile}>
              <Link to="/profile">
                <Avatar
                  alt="Avatar image"
                  className={classes.avatar}
                  src={avatarUrl || dummyAvatar}
                />
              </Link>
              <Typography
                className={classes.nameText}
                variant="h6"
              >
                {name}
              </Typography>
            </div>
          )}
          <Divider />
          <List>
            <MenuDrawerItem
              label={t('Conferences')}
              icon={<ExpensesIcon />}
              path="/conferences"
            />
          </List>
          <Divider />
          {isLogged && (
            <List>
              <MenuDrawerItem
                label={t('Logout')}
                icon={<LogoutIcon />}
                onItemClick={AuthService.logout}
              />
            </List>
          )}
        </div>
      </div>
    </Drawer>
  );
};
