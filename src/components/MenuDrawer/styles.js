import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles((theme) => ({
  list: {
    width: 250,
  },
  toolbar: {
    ...theme.mixins.toolbar,
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(2),
  },
  profile: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    minHeight: 'fit-content',
    paddingTop: '1em',
    paddingBottom: '1em',
  },
  avatar: {
    width: '100px',
    height: '100px',
  },
  nameText: {
    marginTop: theme.spacing(2),
  },
  bioText: {},
}));

export default useStyles;
