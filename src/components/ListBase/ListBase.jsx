import React from 'react';
import PropTypes from 'prop-types';
import List from '@material-ui/core/List';
import { ListBaseItem } from './components/ListBaseItem';

export const ListBase = ({
  items,
  keyProp,
  className,
  ...props
}) => (
  <List className={className}>
    {items.map((item, index) => (
      <ListBaseItem key={item[keyProp]} item={item} index={index} {...props} />
    ))}
  </List>
);

ListBase.propTypes = {
  renderAvatar: PropTypes.func,
  renderAction: PropTypes.func,
  renderPrimary: PropTypes.func.isRequired,
  renderSecondary: PropTypes.func,
  onItemClick: PropTypes.func,
  items: PropTypes.arrayOf(PropTypes.shape()),
  keyProp: PropTypes.string,
  className: PropTypes.string,
  primaryTypographyProps: PropTypes.shape(),
  secondaryTypographyProps: PropTypes.shape(),
  fetchMore: PropTypes.func,
};

const dummyFunc = () => {};
ListBase.defaultProps = {
  renderAvatar: null,
  renderAction: null,
  renderSecondary: null,
  className: null,
  onItemClick: dummyFunc,
  items: [],
  keyProp: 'id',
  primaryTypographyProps: null,
  secondaryTypographyProps: null,
  fetchMore: dummyFunc,
};
