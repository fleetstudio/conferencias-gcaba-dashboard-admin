import React, {
  useState, useMemo, useEffect, useCallback,
} from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import { MenuDrawer } from '../MenuDrawer';
import MenuDrawerContext from '../MenuDrawer/context';
import { Routes } from '../../routes/Routes';
import { AuthService } from '../../services/auth';
import { AppNavBar } from './components/AppNavBar';
import { LoadingModal } from '../LoadingModal';
import { Api } from '../../services/api';
import { Login } from '../Login';
import './style.css';

export const App = () => {
  const [userInfo, setUserInfo] = useState(null);
  const [isOpen, set] = useState(false);
  const [isLogged, setIsLogged] = useState(AuthService.isAuthenticated());
  const [isLoading, setIsLoading] = useState(false);
  const fetchSignedConferences = useCallback(async () => {
    try {
      if (AuthService.isAuthenticated()) {
        const signedConferences = await Api.getUserSignedConferences();
        setUserInfo((prev) => ({
          ...prev, signedConferences,
        }));
      }
      return true;
    } catch (e) {
      return e;
    }
  }, [setUserInfo]);
  const fetchUserData = useCallback(async () => {
    try {
      if (AuthService.isAuthenticated()) {
        const user = await Api.getUserData();
        if (!user || +user.role_id !== 2) {
          AuthService.logout();
        } else {
          user.signedConferences = [];
          setUserInfo(user);
        }
      }
      return true;
    } catch (e) {
      return e;
    }
  }, [setUserInfo]);
  useEffect(() => { fetchUserData(); }, [fetchUserData]);
  const provider = useMemo(() => ({
    isOpen,
    set,
    isLogged,
    setIsLogged,
    userInfo,
    setUserInfo,
    isLoading,
    setIsLoading,
    fetchUserData,
    fetchSignedConferences,
  }), [isOpen,
    set,
    isLogged,
    setIsLogged,
    userInfo,
    setUserInfo,
    isLoading,
    setIsLoading,
    fetchUserData,
    fetchSignedConferences,
  ]);
  // did mount
  useEffect(() => {
    const unsubscribe = AuthService.subscribe('any', (isAuthenticated) => {
      setIsLogged(isAuthenticated);
      if (!isAuthenticated) {
        setUserInfo(null);
      }
    });
    return unsubscribe;
  }, [setIsLogged, setUserInfo]);
  // Initial get user data

  return (
    <MenuDrawerContext.Provider value={provider}>
      <CssBaseline />
      <MenuDrawer />
      <AppNavBar />
      {!isLogged ? (
        <Login />
      ) : (
        <Routes />
      )}
      {isLoading && <LoadingModal />}
    </MenuDrawerContext.Provider>
  );
};
