import React, { useCallback } from 'react';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery/useMediaQuery';
import IconButton from '@material-ui/core/IconButton';
import { Navbar } from '../../Navbar';
import { MenuButton } from '../../MenuButton';
import { AccountButton } from '../../AccountButton';
import { useTranslation } from '../../../services/translation';
import { AuthService } from '../../../services';
import { LanguageButton } from '../../LanguageButton';
import LogoMinisterio from '../../../assets/images/ba-header.svg';

const useStyles = makeStyles((theme) => ({
  title: {
    cursor: 'pointer',
  },
  contentTitle: {
    display: 'flex',
    alignItems: 'center',
  },
  divider: {
    height: 48,
    width: 1,
    color: theme.palette.primary.contrastText,
    backgroundColor: theme.palette.primary.contrastText,
  },
  loginIcon: {
    color: theme.palette.primary.contrastText,
  },
  logo: {
    paddingBottom: 0,
  },
  actionButton: {
    marginRight: 20,
  },
}));

export const AppNavBar = () => {
  const history = useHistory();
  const { t } = useTranslation();
  const matches = useMediaQuery((theme) => theme.breakpoints.up('sm'));
  const classes = useStyles();
  // Title
  const onTitleClick = useCallback(() => history.push('/conferences'), [history]);
  const title = (
    <div className={classes.contentTitle}>
      <IconButton onClick={onTitleClick} className={classes.logo}>
        <img src={LogoMinisterio} alt="logo" />
      </IconButton>
      {matches && (
        <>
          <Divider className={classes.divider} variant="middle" />
          <h2 role="presentation" className={classes.title} onClick={onTitleClick}>{t('Institute Admin')}</h2>
        </>
      )}
    </div>
  );
  // Account button
  const onProfileClick = useCallback(() => history.push('/profile'), [history]);
  const onLogoutClick = useCallback(() => {
    AuthService.logout();
  }, []);
  const accountButton = (
    <AccountButton onProfileClick={onProfileClick} onLogoutClick={onLogoutClick} />
  );
  const authButton = AuthService.isAuthenticated() && accountButton;
  const right = (
    <Grid spacing={1} container alignItems="center" className={matches ? classes.actionButton : ''}>
      <Grid item>
        <LanguageButton />
      </Grid>
      <Grid item>
        {authButton}
      </Grid>
    </Grid>
  );
  return (
    <Navbar left={<MenuButton />} title={title} right={right} />
  );
};

AppNavBar.propTypes = {};

AppNavBar.defaultProps = {};
