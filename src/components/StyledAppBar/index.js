import { withStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar';

export const StyledAppBar = withStyles(theme => ({
  root: {
    backgroundColor: 'white',
    borderTop: `1rem solid ${theme.palette.primary.main}`,
  }
}))(AppBar);
