import React from 'react';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/styles';
import { Typography } from '@material-ui/core';
import { lightGrey } from '../../theme/colors';
import Logo from '../../assets/images/ba_vamosba_blanco.png';

const useStyles = makeStyles((theme) => ({
  rootContainer: {
    backgroundColor: '#333',
    width: '100%',
  },
  preFooter: {
    backgroundColor: '#f5f5f5',
    width: '100%',
    height: '2.5rem',
    textAlign: 'center',
  },
  rootFooter: {
    minHeight: '10rem',
  },
  firstText: {
    color: theme.palette.secondary.contrastText,
  },
  secondText: {
    color: lightGrey,
  },
  containerSecondaryText: {
    textAlign: 'end',
    paddingRight: 15,
  },
  textWithLogoContainer: {
    color: theme.palette.secondary.contrastText,
    display: 'flex',
    alignItems: 'center',
  },
  tinnyText: {
    color: '#717170',
  },
}));

export const Footer = () => {
  const classes = useStyles();
  return (
    <Grid
      container
      className={classes.rootContainer}
    >
      <Grid
        item
        xs={12}
        className={classes.preFooter}
      >
        <h3 className={classes.tinnyText}>Hecho en PHINX</h3>
      </Grid>
      <Grid container className={classes.rootFooter} justify="center" alignItems="center">

        <Grid item xs={12} sm={6} className={classes.textWithLogoContainer}>
          <img src={Logo} alt="logo" />
          <h3>Gobierno de la ciudad</h3>
        </Grid>
        <Grid item xs={12} sm={6} className={classes.containerSecondaryText}>
          <Typography className={classes.firstText}>
            Dirección General de Gestión Digital
          </Typography>
          <Typography className={classes.secondText}>
            Ministerio de Modernización, Innovación y Tecnología
          </Typography>
        </Grid>
      </Grid>
    </Grid>
  );
};
