import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import useMediaQuery from '@material-ui/core/useMediaQuery/useMediaQuery';
import { makeStyles } from '@material-ui/styles';
import { useTranslation } from '../../services/translation';
import confirmDialog from '../../services/confirmDialog';

const isAfterNow = (conference) => moment(conference.date).isSameOrAfter(moment());

const useStyles = makeStyles({
  message: {
    marginRight: 8,
  },
});

export const UnsubscribeAction = ({
  conference, onClick, flipBreakpoint, messageProps,
}) => {
  const classes = useStyles();
  const { t } = useTranslation();
  const matches = useMediaQuery((theme) => theme.breakpoints.up('sm'));

  const isExpired = !isAfterNow(conference);
  const message = isExpired && (
    <Typography
      variant="caption"
      color="error"
      className={classes.message}
      {...messageProps}
    >
      {t('Expired conference')}
    </Typography>
  );

  const onSignUpClick = useCallback(async () => {
    const confirm = await confirmDialog.show({
      title: t('Unsubscribe Confirmation'),
      content: `${t('Do you want to remove your inscription for')} "${conference.name}" ?`,
      confirmText: `${t('Unsubscribe')}!`,
      cancelText: t('No'),
    });
    if (confirm && onClick) {
      onClick(conference);
    }
  }, [conference, onClick, t]);
  const shouldRender = flipBreakpoint ? !matches : matches;
  return shouldRender && (
    <>
      {message}
      <Button
        disabled={isExpired}
        variant="contained"
        color="secondary"
        onClick={onSignUpClick}
      >
        {t('Unsubscribe')}
      </Button>
    </>
  );
};

UnsubscribeAction.propTypes = {
  conference: PropTypes.shape().isRequired,
  messageProps: PropTypes.shape(),
  onClick: PropTypes.func,
  flipBreakpoint: PropTypes.bool,
};

UnsubscribeAction.defaultProps = {
  onClick: null,
  flipBreakpoint: false,
  messageProps: {},
};
