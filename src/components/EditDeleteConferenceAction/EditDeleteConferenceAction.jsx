import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import IconButton from '@material-ui/core/IconButton';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import EditIcon from '@material-ui/icons/Edit';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import confirmDialog from '../../services/confirmDialog';
import { useTranslation } from '../../services/translation';

export const EditDeleteConferenceAction = ({
  onEditClick, onRemoveClick, conference, className, flipBreakpoint,
}) => {
  const { t } = useTranslation();
  const matches = useMediaQuery((theme) => theme.breakpoints.up('sm'));
  const onEdit = useCallback(() => onEditClick(conference), [onEditClick, conference]);
  const onRemove = useCallback(async () => {
    const confirm = await confirmDialog.show({
      title: t('Remove Confirmation'),
      content: `${t('Do you want to remove')} "${conference.name}" ?`,
      confirmText: `${t('Remove it')}!`,
      cancelText: t('No'),
    });
    if (confirm) {
      onRemoveClick(conference);
    }
  }, [onRemoveClick, conference, t]);
  const shouldRender = flipBreakpoint ? !matches : matches;
  return shouldRender && (
    <div className={className}>
      <IconButton onClick={onEdit}>
        <EditIcon />
      </IconButton>
      <IconButton onClick={onRemove}>
        <DeleteForeverIcon />
      </IconButton>
    </div>
  );
};

EditDeleteConferenceAction.propTypes = {
  className: PropTypes.string,
  onEditClick: PropTypes.func,
  onRemoveClick: PropTypes.func,
  conference: PropTypes.oneOfType([PropTypes.shape(), PropTypes.node]),
  flipBreakpoint: PropTypes.bool,
};

const dummy = () => {};
EditDeleteConferenceAction.defaultProps = {
  className: '',
  onEditClick: dummy,
  onRemoveClick: dummy,
  conference: null,
  flipBreakpoint: false,
};
