import { useForm } from 'react-final-form-hooks';
import { useCallback } from 'react';
import { FORM_ERROR } from 'final-form';
import { useSnackbar } from 'notistack';
import { getErrorMessage, makeValidator } from '../../../utils/helpers';
import { useTranslation } from '../../../services/translation';
import { useGlobalContext } from '../../../hooks';
import { Api } from '../../../services/api';
import { AuthService } from '../../../services';

const validate = makeValidator([
  { key: 'email', validators: ['required', 'email'] },
  { key: 'password', validators: ['required'] },
]);

export const useLoginForm = () => {
  const { setUserInfo } = useGlobalContext();
  const { enqueueSnackbar } = useSnackbar();
  const { t } = useTranslation();

  const onSubmit = useCallback(async (values) => {
    try {
      const user = await Api.login(values);
      if (!user || +user.role_id !== 2) {
        AuthService.logout();
        const message = 'Permission denied!';
        enqueueSnackbar(t(message), { variant: 'error' });
        return { [FORM_ERROR]: message };
      }
      user.signedConferences = [];
      setUserInfo(user);

      return {};
    } catch (e) {
      const message = getErrorMessage(e);
      enqueueSnackbar(t(message || 'Something is wrong'), { variant: 'error' });
      return { [FORM_ERROR]: message };
    }
  }, [t, enqueueSnackbar, setUserInfo]);

  return useForm({
    initialValues: { email: '', password: '' },
    onSubmit,
    validate,
  });
};
