import React from 'react';
import { Screen } from '../Screen';
import { LoginForm } from './components/LoginForm';

export const Login = () => (
  <Screen maxWidth="xs">
    <LoginForm />
  </Screen>
);

Login.propTypes = {};

Login.defaultProps = {};
