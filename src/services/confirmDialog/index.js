import ServiceComponent from './ServiceComponent';
import theme from '../../theme';

export default ServiceComponent.create({ customTheme: theme });
