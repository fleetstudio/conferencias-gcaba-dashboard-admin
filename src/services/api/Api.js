import axios from 'axios';
import { API_URL } from '../../config';
import { AuthService } from '../auth';

class ApiRaw {
  constructor() {
    this.axios = axios.create({
      baseURL: API_URL,
    });
    this.axios.interceptors.request.use(ApiRaw.addTokenHeader);
    this.axios.interceptors.response.use(ApiRaw.successHandler, ApiRaw.errorHandler);
  }

  static addTokenHeader(request) {
    const token = AuthService.getToken();
    if (token) request.headers.Authorization = `Bearer ${token}`;
    return request;
  }

  static errorHandler(error) {
    if (error.response.status === 401) {
      AuthService.logout();
    }
    const returnedError = error.response.data
    && (error.response.data.msg || error.response.data.message)
      ? new Error(error.response.data.msg || error.response.data.message)
      : error;
    return Promise.reject(returnedError);
  }

  static successHandler(response) {
    if (!response.data || !response.data.data) return response.data;
    if (response.data.data.access_token) {
      AuthService.login(response.data.data.access_token);
    }
    return response.data.data;
  }

  login({ email, password }) {
    return this.axios.post('/login', { email, password });
  }

  register({
    // eslint-disable-next-line camelcase
    email, password, password_confirmation, name,
  }) {
    return this.axios.post('/register', {
      email, password, password_confirmation, name,
    });
  }

  getUserData() {
    return this.axios.get('/me');
  }

  getConferences() {
    return this.axios.get('/conferences');
  }

  getConferenceById(conferenceId) {
    // TODO: change for conferences
    return this.axios.get(`/conferences/${conferenceId}`);
  }

  getSignedUsersByConference(conferenceId) {
    // TODO: change for conferences
    return this.axios.get(`/conferences/${conferenceId}/users`);
  }

  createConference(values) {
    return this.axios.post('/conferences', values);
  }

  updateConferenceById(conferenceId, values) {
    return this.axios.put(`/conferences/${conferenceId}`, values);
  }

  removeConferenceById(conferenceId) {
    return this.axios.delete(`/conferences/${conferenceId}`, {});
  }

  removeInscriptionById(conferenceId, userId) {
    return this.axios.delete(`/conferences/${conferenceId}/${userId}/register`, {});
  }

  getUserSignedConferences() {
    return this.axios.get('/me/conferences', {});
  }

  removeConferenceSubscription(conferenceId) {
    return this.axios.delete(`/conferences/${conferenceId}/register`, {});
  }

  addConferenceSubscription(conferenceId) {
    return this.axios.post(`/conferences/${conferenceId}/register`, { conference_id: conferenceId });
  }
}

export const Api = new ApiRaw();
