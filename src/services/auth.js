let loginSubscriptions = [];
let logoutSubscriptions = [];

export class AuthService {
  static setAuthType(authType) {
    if (authType) localStorage.setItem('authType', authType);
  }

  static login(token, authType = null) {
    localStorage.setItem('access_token', token);
    AuthService.setAuthType(authType);
    loginSubscriptions.forEach((func) => func(AuthService.isAuthenticated(), token));
  }

  static logout() {
    localStorage.removeItem('access_token');
    localStorage.removeItem('authType');
    logoutSubscriptions.forEach((func) => func(AuthService.isAuthenticated()));
  }

  static getToken() {
    return localStorage.getItem('access_token');
  }

  static isAuthenticated() {
    return !!AuthService.getToken();
  }

  static subscribe(event, func) {
    if (typeof func !== 'function') return false;
    if (event === 'any') {
      loginSubscriptions = [...loginSubscriptions, func];
      logoutSubscriptions = [...logoutSubscriptions, func];
      return () => {
        loginSubscriptions = loginSubscriptions.filter((item) => item !== func);
        logoutSubscriptions = logoutSubscriptions.filter((item) => item !== func);
      };
    }
    if (event === 'login') {
      loginSubscriptions = [...loginSubscriptions, func];
      return () => {
        loginSubscriptions = loginSubscriptions.filter((item) => item !== func);
      };
    }
    if (event === 'logout') {
      logoutSubscriptions = [...logoutSubscriptions, func];
      return () => {
        logoutSubscriptions = logoutSubscriptions.filter((item) => item !== func);
      };
    }
    return false;
  }
}
